# Unico
This project is created as coding test for Unico
It is a single page application, written in typescript using angular 1.6 framework. 

Features
- Dependency packages are maintained in npm
- Developed, packaged, and optimized using webpack 2.0
- SASS pre-processor


## Usage
### Installation
- Open the folder in a console
- Install global dependencies, `npm install -g gulp`
- Run `npm install`, to install all dependencies (ignore warnings, if any)
- Run `npm run serve`
- Navigate to `http://localhost:3000`

To serve an optimized version, run `npm run serve:dist`

### Build
Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Run tests
Run `npm run test` will execute all unit tests, (*.spec.ts)

## Online Demo
AWS hosted cloudfront distribution [Demo](https://unico.jeffreyvictorino.com) or [S3 bucket](http://unico.jeffreyvictorino.com.s3-website-ap-southeast-1.amazonaws.com/)

#### AWS Infrastructure
- Optimized version is hosted in an S3 bucket
- S3 bucket is behind a CloudFront distribution
- Domain is managed in Route 53 service