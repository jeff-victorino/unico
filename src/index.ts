import * as angular from 'angular';

import './dependencies';
import { config } from './config';
import { appComponent } from './app/app';
import routesConfig from './routes';
import * as components from './app/components';
import { ProductService } from './app/services';
export const app: string = 'app';

var appModule = angular
  .module(app, ['ui.router', 'ngMaterial', 'ngMessages', 'angular.filter']);

appModule
  .config(routesConfig)
  .config(config)
  .service('productService', ProductService)

  .component('app', appComponent)
  .component('uncHeader', components.headerComponent)
  .component('uncProduct', components.productComponent)
  .component('uncCart', components.cartComponent)
  ;
