export var config = ($mdThemingProvider: any) => {
    'ngInject';
    var unicoMap = $mdThemingProvider.extendPalette('teal', {
        '500': '#4BA7A9'
    });
    $mdThemingProvider.definePalette('unico', unicoMap);
    $mdThemingProvider.theme('default')
        .primaryPalette('unico');
};
