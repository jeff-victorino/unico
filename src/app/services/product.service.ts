import { IItem } from '../models';

export class ProductService {
    private cartItems: Array<IItem> = new Array();

    constructor(private $http: any, private $rootScope: any) {
        'ngInject';
    }

    getProducts() {
        return this.$http.get('/app/services/payload.json')
            .then(res => {
                return res.data.products;
            });
    }

    getCartItems() {
        return this.cartItems;
    }

    addItem(item: IItem) {
        let existingItem = this.cartItems.find(i => i.id === item.id);

        if (existingItem) {
            existingItem.noOfCartons = ( existingItem.noOfCartons || 0 ) + 1;
        } else {
            item.noOfCartons = 1;
            this.cartItems.push(item);
        }

        this.onItemsChanged();
    }

    removeItem(item: IItem) {
        this.cartItems = this.cartItems.filter(i => i.id !== item.id);
        this.onItemsChanged();
    }

    onItemsChanged() {
        this.$rootScope.$emit('items-changed', this.cartItems);
    }
}
