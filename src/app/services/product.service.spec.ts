import * as angular from 'angular';
import 'angular-mocks';
import { ProductService } from './product.service';

describe('Product Service', () => {
  let service,
    $http,
    $rootScope,
    $q;

  beforeEach(() => {
    angular
      .module('test', [])
      .service('service', ProductService);
    angular.mock.module('test');
  });

  beforeEach(inject((_service_, _$http_, _$rootScope_, _$q_) => {
    $q = _$q_;
    $http = _$http_;
    $rootScope = _$rootScope_;
    service = _service_;
  }));

  describe('getProducts', () => {
    it('should call api', () => {
      // usually we setup httpBackend to prevent the test to make actual calls
      // but since its a mock json, its fine.
      spyOn($http, 'get').and.returnValue($q.when({ data: { products: [] } }));

      service.getProducts();
      $rootScope.$digest();

      expect($http.get).toHaveBeenCalledWith('/app/services/payload.json');
    });

    it('should return a promise', () => {
      let result = service.getProducts();
      expect(result.then).toBeDefined();
    });
  });

  describe('getCartItems', () => {
    it('should return item array', () => {
      let expected = [{}, {}, {}];
      service.cartItems = expected;

      let result = service.getCartItems();
      expect(result).toBe(expected);
    });
  });

  describe('addItem', () => {
    let item = {
      id: 1,
      title: 'test item'
    };
    it('should push new item', () => {
      service.addItem(item);
      expect(service.cartItems).toContain(item);
    });

    it('should update count when exists', () => {
      service.addItem(item);
      service.addItem(item);
      expect(service.cartItems[0].noOfCartons).toBe(2);
    });

    it('should call onItemsChange', () => {
      spyOn(service, 'onItemsChanged');
      service.addItem(item);
      expect(service.onItemsChanged).toHaveBeenCalled();
    });
  });

  describe('removeItem', () => {
    it('should remove item from array', () => {
      let items = [{ id: 1 }, { id: 2 }, { id: 3 }];
      service.cartItems = items;

      service.removeItem(items[0]);

      expect(service.cartItems.length).toBe(2);
      expect(service.cartItems.filter(i => i.id === 1)).toEqual([]);
    });

    it('should call onItemsChange', () => {
      spyOn(service, 'onItemsChanged');
      service.removeItem({ id: 1 });
      expect(service.onItemsChanged).toHaveBeenCalled();
    });
  });

  describe('onItemsChanged', () => {
    it('should emit event', () => {
      spyOn($rootScope, '$emit');
      service.onItemsChanged();
      expect($rootScope.$emit).toHaveBeenCalledWith('items-changed', []);
    });
  });
});
