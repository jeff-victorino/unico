
export interface IRecommendation {
    currentLevel: number;
    maxLevel: 100;
}

export interface IItem {
    id: number;
    title: string;
    category: string;
    imageUrl: string;
    unitsInCartons: number;
    noOfCartons: number;
    unitCost: number;
    packSize: number;
    secondaryCategory: string;
}

export interface IProduct {
    recommendation: IRecommendation;
    items: Array<IItem>;
}
