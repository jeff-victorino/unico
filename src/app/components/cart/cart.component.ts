import './cart.scss';
import { ProductService } from '../../services';
import { IItem } from '../../models';

export class CartController {
    items: Array<IItem>;

    constructor(private productService: ProductService) {
        'ngInject';
    }

    $onInit() {
        this.items = this.productService.getCartItems();
    }

    removeItem(item: IItem) {
        this.productService.removeItem(item);
        this.items = this.productService.getCartItems();
    }

    itemChanged() {
        this.productService.onItemsChanged();
    }
}

export const cartComponent: angular.IComponentOptions = {
    template: require('./cart.html'),
    controller: CartController,
    controllerAs: 'vm'
};

