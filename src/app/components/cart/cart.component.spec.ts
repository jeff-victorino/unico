import { cartComponent } from './cart.component';
import * as angular from 'angular';
import 'angular-mocks';

describe('cartComponent', () => {
    let vm,
        element,
        $rootScope,
        mockProductService;

    beforeEach(() => {
        mockProductService = jasmine.createSpyObj('mockProductService', ['getCartItems', 'removeItem', 'onItemsChanged']);

        cartComponent.template = '<div> cart template</div>';

        angular.module('test', [])
            .component('cart', cartComponent);

        angular.mock.module('test', $provide => {
            $provide.value('productService', mockProductService);
        });
    });

    beforeEach(inject((_$rootScope_, $compile) => {
        $rootScope = _$rootScope_;

        element = angular.element('<cart></cart>');
        $compile(element)($rootScope.$new());
        $rootScope.$digest();
        vm = element.isolateScope().vm;

        mockProductService.getCartItems.and.returnValue([{}, {}, {}]);
    }));

    it('should be compiled', () => {
        expect(element).not.toEqual(null);
    });

    describe('$onInit', () => {
        it('should get items', () => {
            vm.$onInit();
            expect(vm.items).toEqual([{}, {}, {}]);
            expect(mockProductService.getCartItems).toHaveBeenCalled();
        });
    });

    describe('removeItem', () => {
        it('should call product service and reset items', () => {
            vm.removeItem({ id: 1234 });

            expect(mockProductService.removeItem).toHaveBeenCalledWith({ id: 1234 });
            expect(vm.items).toEqual([{}, {}, {}]);
            expect(mockProductService.getCartItems).toHaveBeenCalled();
        });
    });

    describe('itemChanged', () => {
        it('should invoke product service', () => {
            vm.itemChanged();
            expect(mockProductService.onItemsChanged).toHaveBeenCalled();
        });
    });

});
