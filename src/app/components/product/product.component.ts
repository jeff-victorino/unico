import './product.scss';
import { ProductService } from '../../services';
import { IProduct, IItem } from '../../models';

export class ProductController {
  products;
  constructor(private productService: ProductService,
              private $state: angular.ui.IStateService) {
                'ngInject';
              }

  $onInit() {
    this.productService.getProducts().then(products => {
      this.products = products;
    });
  }

  isShowFooter(product: IProduct) {
    return product.items && product.items.length > 1;
  }

  addItem(item: IItem) {
    this.productService.addItem(item);
    this.$state.go('cart');
  }

  addProductItems(product: IProduct) {
    for (let item of product.items) {
      this.productService.addItem(item);
    }
    this.$state.go('cart');
  }
}

export const productComponent: angular.IComponentOptions = {
  template: require('./product.html'),
  controller: ProductController,
  controllerAs: 'vm'
};
