import { productComponent } from './product.component';
import * as angular from 'angular';
import 'angular-mocks';

// another way to mock a dependency is to create a mock object manually
export class MockProductService {
    constructor(private $q: any) {
        'ngInject';
    }
    getProducts() {
        return this.$q.when({});
    }

    addItem(item: any) {
        // mock
    }
}

describe('productComponent', () => {
    let vm,
        element,
        $rootScope,
        $q,
        mockProductService,
        mockState;

    beforeEach(() => {
        mockState = jasmine.createSpyObj('mockSate', ['go']);

        productComponent.template = '<div> product template</div>';

        angular.module('test', [])
            .component('product', productComponent)
            .service('productService', MockProductService);

        angular.mock.module('test', $provide => {
            $provide.value('$state', mockState);
        });
    });

    beforeEach(inject((_$rootScope_, _$q_, $compile, _productService_) => {
        $rootScope = _$rootScope_;
        $q = _$q_;
        mockProductService = _productService_;

        element = angular.element('<product></product>');
        $compile(element)($rootScope.$new());
        $rootScope.$digest();
        vm = element.isolateScope().vm;

        spyOn(mockProductService, 'getProducts').and.returnValue($q.when({}));
        spyOn(mockProductService, 'addItem');
    }));

    describe('$onInit', () => {
        it('should get products', () => {
            vm.$onInit();
            expect(mockProductService.getProducts).toHaveBeenCalled();
        });
    });

    describe('addItem', () => {
        it('should call product service and change state', () => {
            vm.addItem({ id: 1234 });

            expect(mockProductService.addItem).toHaveBeenCalledWith({ id: 1234 });
            expect(mockState.go).toHaveBeenCalledWith('cart');
        });
    });

    describe('addProductItems', () => {
        it('should add all items and change state', () => {
            vm.addProductItems({ items: [{ id: 1234 }, { id: 4567 }] });

            expect(mockProductService.addItem).toHaveBeenCalledTimes(2);
            expect(mockState.go).toHaveBeenCalledWith('cart');
        });
    });

});
