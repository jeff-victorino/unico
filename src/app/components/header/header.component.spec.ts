import { headerComponent } from './header.component';
import * as angular from 'angular';
import 'angular-mocks';

describe('headerComponent', () => {
    let vm,
        element,
        $rootScope;

    beforeEach(() => {
        angular.module('test', [])
            .component('header', headerComponent);

        angular.mock.module('test');
    });

    beforeEach(inject((_$rootScope_, $compile) => {
        $rootScope = _$rootScope_;

        element = angular.element('<header></header>');
        $compile(element)($rootScope.$new());
        $rootScope.$digest();
        vm = element.isolateScope().vm;
    }));

    it('should be compiled', () => {
        expect(element).not.toEqual(null);
    });

    describe('$onInit', () => {
        it('should register on items-changed event', () => {
            spyOn($rootScope, '$on');

            vm.$onInit();
            expect($rootScope.$on).toHaveBeenCalled();
        });

        it('should set count and amount of emit', () => {
            spyOn(vm, 'getTotalAmount').and.returnValue(200);

            vm.$onInit();

            $rootScope.$emit('items-changed', [{}, {}]);

            expect(vm.getTotalAmount).toHaveBeenCalled();
            expect(vm.amount).toBe(200);
            expect(vm.count).toBe(2);
        });
    });

    describe('getTotalAmount', () => {
        it('should return total amount', () => {
            let items = [
                {
                    id: 1,
                    unitCost: 123,
                    unitsInCartons: 100,
                    noOfCartons: 10
                },
                {
                    id: 2,
                    unitCost: 1,
                    unitsInCartons: 1,
                    noOfCartons: 1
                }
            ];

            let result = vm.getTotalAmount(items);

            expect(result).toBe(123001);
        });

        it('should return 0 when no noOfCartons', () => {
            let items = [
                {
                    id: 1,
                    unitCost: 123,
                    unitsInCartons: 100
                }
            ];

            let result = vm.getTotalAmount(items);

            expect(result).toBe(0);
        });
    });
});
