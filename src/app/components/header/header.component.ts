import './header.scss';
import { IItem } from '../../models';

export class HeaderController {
  count: number = 0;
  amount: number = 0;
  currency: string = '$';

  constructor(private $rootScope: angular.IRootScopeService) {
    'ngInject';
   }

  $onInit() {
      this.$rootScope.$on('items-changed', (event, items) => {
        this.count = items.length;
        this.amount = this.getTotalAmount(items);
    });
  }

  getTotalAmount(items: Array<IItem>): number {
    let total: number = 0;
    for (let item of items) {
      let itemTotal = item.unitCost * item.unitsInCartons * (item.noOfCartons || 0);
      total = total + itemTotal;
    }

    return total;
  }
}

export const headerComponent: angular.IComponentOptions = {
  template: require('./header.html'),
  controller: HeaderController,
  controllerAs: 'vm'
};
